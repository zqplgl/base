# from distutils.core import setup
from setuptools import setup


setup(name='demo',
      version='2.0.0',
      author="zqp",
      author_email="zqp@qq.com",
      url="hello",
      packages=["demo"],
      install_requires=["numpy>1.17", "opencv-python"],
      zip_safe=False
      )
